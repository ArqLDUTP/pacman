## Import functions required
from utils.drawModel import drawModel
from utils.eatCookies import eatCookies
from utils.openMatrix import openMatrix
from utils.killPlayer import killPlayer
from utils.killSomePlayer import killSomePlayer
from utils.getNewPosPlayer import getNewPosPlayer
from utils.getAllBlockSprites import getAllBlockSprites

# Import Sprite models
from models.block import Block
from models.player import Player

# Import ZMQ, sys and pygame
import zmq          # Socket connection
import sys          # Validator arguments
import pygame       # Graphic library

if len(sys.argv) != 3:      # Validate command line with args
    # If not valid command line
    # Print error message
    print("Sample call: python pacman.py <username> <server_address>")
    # Exit app
    exit()

# Get username from command line
username        = str(sys.argv[1])
# Get server address from command line
serverAddress   = str(sys.argv[2])

# Define colors
BLACK           = (0,0,0)
BLOCK_COLOR     = (25, 25, 140)     # Dark_blue
COIN_COLOR      = (255, 159, 149)   # Misty Rose

# Define vars to get from server
# SIZE_BLOCK -> int -> Define square size
SIZE_BLOCK      = None
# window_size -> pair -> (width, heigth)
window_size     = None
# matrix -> lists in list -> Data blocks (1 -> Block)
matrix          = None
# ROWS -> int -> Number of rows
ROWS            = None
# COLS -> int -> Number of cols
COLS            = None
# ENTITY_TYPE   -> int
#                   -> 0 -> Pacman
#                   -> 1 -> Red ghost
#                   -> 2 -> Light blue ghost
#                   -> 3 -> Orange ghost
ENTITY_TYPE     = None
# Local User Points
USER_POINTS     = 0

# Create ZMQ Context
context         = zmq.Context()
# Create dealer socket
socket          = context.socket(zmq.DEALER)
# Set username as socket's identity
socket.identity = username.encode('ascii')
# Connect socket to serverAddress
socket.connect("tcp://" + serverAddress)
# New user connected's notification to server
socket.send_multipart([b'player_connection'])
# Message info
print("Started client with id: %s" % username)

# Create a ZMQ Poller
poller          = zmq.Poller()
# Sign up socket in poller
poller.register(socket, zmq.POLLIN)

# Flag to wait data
waitingData     = True

# Wait first data sended by server
while waitingData:
    # Get sockets with events
    socks       = dict(poller.poll())
    # If serverSocket has some info sended
    if socket in socks:
        # Get event sended by server
        ident, *rest    = socket.recv_multipart()
        # Get window_size and eval to cast it to list of lists
        window_size     = eval(rest[0].decode('ascii'))
        # Get matrix and eval to cast it to list of lists
        matrix          = eval(rest[1].decode('ascii'))
        # Get coins_collected and eval to cast it to dict
        coins_collected = eval(rest[2].decode('ascii'))
        # Get users_connected and eval to cast it to dict
        users_connected = eval(rest[3].decode('ascii'))
        # Get number of rows and cast it to int
        ROWS            = int(rest[4].decode('ascii'))
        # Get number of cols and cast it to int
        COLS            = int(rest[5].decode('ascii'))
        SIZE_BLOCK      = int(rest[6].decode('ascii'))
        # Get entity type assigned of rows and cast it to int
        ENTITY_TYPE     = int(rest[7].decode('ascii'))
        # Stop waitting for info
        waitingData     = False

# Init Pygame
pygame.init()
# Create screen with window_size
screen              = pygame.display.set_mode(window_size)
# Clean screen with black background
screen.fill(BLACK)
# Set title to window as example: "Pacman: User1"
pygame.display.set_caption("Pacman: " + username)

# Set flaf if window if running
RUNNING_WINDOW      = True
# Create pygmae clock
PYGAME_CLOCK        = pygame.time.Clock()

# Get:
#   -> Group Entities of blocks with their positions
#   -> Coins List determinate by
#        -> (space) -> Normal coin site
#        -> C       -> Power coin site
blocks, coins       = getAllBlockSprites(matrix, BLOCK_COLOR, COIN_COLOR, SIZE_BLOCK)

# Total number coins
NUM_COINS           = len(coins)

# Default init user/player position in matrix
POS_PLAYER          = (16, 16)

# Create entities group (Created for players entities)
entities            = pygame.sprite.Group()

# Create dict of players
players             = {}
# In key[username] save Player object Sprite
players[username]   = Player(
    POS_PLAYER,
    BLACK,
    'left',
    SIZE_BLOCK,
    ENTITY_TYPE,
    username
)

# Add local player in entities
entities.add(players[username])

# Create users and add to entities
for userKey in users_connected:
    # Get info user by username like key in users_connected
    infoUser            = users_connected[userKey]

    # Get username in string
    userConnected       = userKey.decode('ascii')
    # Get X position user
    userPosX            = infoUser['pos']['x']
    # Get Y position user
    userPosY            = infoUser['pos']['y']
    # Create pair/location of user
    userPos             = (userPosX, userPosY)
    # Get type of entity user
    typeUser            = infoUser['type']

    # In key[username] save Player object Sprite
    players[userConnected] = Player(
        userPos,
        BLACK,
        'left',
        SIZE_BLOCK,
        typeUser,
        userConnected
    )

    # Add local player in entities
    entities.add(players[userConnected])

# Start window pygame cicle
while RUNNING_WINDOW:
    # Try to get event in socket without blocking event
    try:
        # Get operantion and rest of info without blocking event
        operation, *rest    = socket.recv_multipart(zmq.NOBLOCK)

        # If socket notified a new user connected
        if operation == b'new_user':
            # Get rest of info received and cast it to list
            rest            = eval(rest[0].decode('ascii'))

            # Get username in string from first position of rest info
            usernameNewUser = rest[0].decode('ascii')
            # Get type of entity user
            userType        = rest[1]
            # Get X position newUser
            posXNewUser     = rest[2]
            # Get Y position newUser
            posYNewUser     = rest[3]
            # Create pair/location of user
            posNewUser      = (posXNewUser, posYNewUser)

            # In key[username] save Player object Sprite
            players[usernameNewUser] = Player(
                posNewUser,
                BLACK,
                'left',
                SIZE_BLOCK,
                userType,
                usernameNewUser
            )

            # Add local player in entities
            entities.add(players[usernameNewUser])

        # If socket notified a someone user has ate a cookie
        if operation == b'cookie_eated':
            # Get str posX of coin ate
            posX            = rest[0].decode('ascii')
            # Get str posY of coin ate
            posY            = rest[1].decode('ascii')
            # Get user who ate the cookie
            userEatedCookie = rest[2].decode('ascii')

            # Create a key for cookie ate
            key             = posX + " " + posY

            # In cookie ate key, set user who ate it
            coins_collected[key] = userEatedCookie

        # If socket notified that some user changed its position
        if operation == b'user_change_position':
            # Get user who changed its position
            usernameUser    = rest[0].decode('ascii')
            # Get new position user pair object
            posUser         = eval(rest[1].decode('ascii'))
            # Get new vector user string
            vectorUser      = rest[2].decode('ascii')

            # Change position to object of user
            players[usernameUser].change_position(posUser, vectorUser)

        # If socket notified that some player has killed
        if operation == b'kill_someone':
            # Get username of killer
            killer          = rest[0].decode('ascii')
            # Get username of user killed
            user_killed     = rest[1].decode('ascii')

            # Validate if I was the murdered user
            if user_killed == username:
                # Message with info of killer
                print("You was killed by %s!" % killer)
                # Stop program
                RUNNING_WINDOW = False
                # Hard stop program
                break
            # If I wasn't the murdered user
            else:
                # Remove player killed from players
                players, entities = killPlayer(players, entities, user_killed)
    # If socket hasn't events
    except zmq.ZMQError as error:
        # Continue with the program
        pass

    # Clean screen to redraw
    screen.fill(BLACK)

    # Detect if player pressed some key
    for event in pygame.event.get():
        # If user close the game
        if event.type == pygame.QUIT:
            # Off the game window
            RUNNING_WINDOW                      = False
        # If user press some key of keyboard
        elif event.type == pygame.KEYDOWN:
            # If user picked up the arrow up
            if event.key==pygame.K_UP:
                # Get new relative position of user
                # Get new relative position of user
                POS_PLAYER                      = getNewPosPlayer(
                    POS_PLAYER, matrix, 'up', ROWS, COLS
                )
                # Update player position
                # Update player position
                players[username].update(POS_PLAYER, 'up', socket)
                # Review if I kill some player
                players, entities, USER_POINTS  = killSomePlayer(
                    players, entities, username, socket, USER_POINTS
                )

            # If user picked up the arrow down
            if event.key==pygame.K_DOWN:
                # Get new relative position of user
                POS_PLAYER                      = getNewPosPlayer(
                    POS_PLAYER, matrix, 'down', ROWS, COLS
                )
                # Update player position
                players[username].update(POS_PLAYER, 'down', socket)
                # Review if I kill some player
                players, entities, USER_POINTS  = killSomePlayer(
                    players, entities, username, socket, USER_POINTS
                )

            # If user picked up the arrow left
            if event.key==pygame.K_LEFT:
                # Get new relative position of user
                POS_PLAYER                      = getNewPosPlayer(
                    POS_PLAYER, matrix, 'left', ROWS, COLS
                )
                # Update player position
                players[username].update(POS_PLAYER, 'left', socket)
                # Review if I kill some player
                players, entities, USER_POINTS  = killSomePlayer(
                    players, entities, username, socket, USER_POINTS
                )

            # If user picked up the arrow right
            if event.key==pygame.K_RIGHT:
                # Get new relative position of user
                POS_PLAYER                      = getNewPosPlayer(
                    POS_PLAYER, matrix, 'right', ROWS, COLS
                )
                # Update player position
                players[username].update(POS_PLAYER, 'right', socket)
                # Review if I kill some player
                players, entities, USER_POINTS  = killSomePlayer(
                    players, entities, username, socket, USER_POINTS
                )

    # Review coins eated
    coins, coins_collected, USER_POINTS         = eatCookies(
        coins, POS_PLAYER, socket, coins_collected, username, USER_POINTS
    )

    # Draw blocks in window
    blocks.draw(screen)
    # Draw coins in screen
    drawModel(screen, coins)
    # Draw players in screen
    entities.draw(screen)

    # Update screen
    pygame.display.update()
    # Set time clock
    PYGAME_CLOCK.tick(60)

# Quit pygame
pygame.quit()
# Show user points
print("%s: %i points!" % (username, USER_POINTS))
