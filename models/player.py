# Import pygame
import pygame

# Player class
class Player(pygame.sprite.Sprite):

    # Constructor
    # -> posPlayer          -> Position of player
    # -> background_color   -> Background color
    # -> vector             -> Direction of player
    # -> SIZE_BLOCK         -> Size of block
    # -> ENTITY_TYPE        -> Type of entity (image to show)
    # -> username           -> Player identify
    def __init__(
        self,
        posPlayer,
        background_color,
        vector,
        SIZE_BLOCK,
        ENTITY_TYPE,
        username
    ):
        # Call Sprite's constructor
        super().__init__()
        # Set SIZE_BLOCK
        self.SIZE_BLOCK         = SIZE_BLOCK
        # Set ENTITY_TYPE
        self.ENTITY_TYPE        = ENTITY_TYPE
        # Set background_color
        self.background_color   = background_color
        # Set vector direction
        self.vector             = vector
        # Set player position
        self.position           = posPlayer
        # Set identity player
        self.identity           = username

        # Create empty field image
        self.image              = None
        # Create empty field rect
        self.rect               = None

        # Assign image with method @update_image
        self.update_image()

    # Get position in pixels of player object
    def get_real_position(self):
        # Create posX in pixels
        posX                    = self.position[0]*self.SIZE_BLOCK
        # Create posY in pixels
        posY                    = self.position[1]*self.SIZE_BLOCK
        # Create pos pair and return it
        return (posX, posY)

    # Update image (or create it)
    def update_image(self):
        # If type entity is Pacman (0)
        if self.ENTITY_TYPE == 0:
            # Set pacman seeing in vector direction image
            self.image  = pygame.image.load('res/pacman/%s.gif' % (self.vector))
        # If type entity isn't Pacman, is a ghost
        else:
            # Set number of ghost image
            self.image  = pygame.image.load('res/ghosts/%i.gif' % (self.ENTITY_TYPE))

        # Set rect object
        self.rect = self.image.get_rect(topleft=self.get_real_position())

    # Change vector direction
    def change_vector(self, newVector):
        # Set new vector
        self.vector             = newVector
        # Update image with change of vector
        self.update_image()

    # Change position and vector player
    def change_position(self, posPlayer, vector):
        # Set new position
        self.position           = posPlayer
        # Set and change vector, and update image
        self.change_vector(vector)

    # Get relative position
    def get_position(self):
        # Return position player
        return self.position

    # Get pos in bytes object
    def getPosInBytes(self):
        # Create var pos mutable
        pos                     = self.position
        # Return pos in bytes object
        return bytes(str(pos), 'ascii')

    # Update position user
    def update(self, posPlayer, vector, socket):
        # Change player position
        self.change_position(posPlayer, vector)
        # Update image
        self.update_image()
        # Create a vector val in bytes
        vectorBytes             = bytes(vector, 'ascii')
        # Send actualization to socket [eventID, pairPosInBytes, vectorInBytes]
        socket.send_multipart([
            b'change_position',
            self.getPosInBytes(),
            vectorBytes
        ])
