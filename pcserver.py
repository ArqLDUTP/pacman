## Import functions required
from utils.openMatrix import openMatrix
from utils.multicastUsers import multicastUsers

# Import ZQM, sys
import zmq          # Socket connection
import sys          # Validator arguments

# Set default pixels size to square
SIZE_BLOCK = 20

# Create a main function
def main():
    # Validate command line with args
    if len(sys.argv) != 3:
        # If not valid command line
        # Print error message
        print("Sample call: python pcserver.py <port> <path_to_matrix>")
        # Exit app
        exit()

    # Get port from command line and cast it to int
    port = int(sys.argv[1])
    # Get matrix resource from command line
    matrixFile = sys.argv[2]

    # Create ZMQ Context
    context = zmq.Context()
    # Create router socket
    socket = context.socket(zmq.ROUTER)
    # Listen socket in port string assigned from command line
    socket.bind("tcp://*:"+str(port))

    # Message success server up
    print("Started server")

    # Define window_size, matrix, number of rows and number of cols
    # from matrix.txt resource
    window_size, matrix, ROWS, COLS     = openMatrix(matrixFile, SIZE_BLOCK)

    # Init dics users_connected and coins_collected
    users_connected                     = {}
    coins_collected                     = {}

    # Keep alive proccess
    while True:
        # Get event in on socket
        ident, operation, *rest = socket.recv_multipart()

        # If new user connected
        if operation == b'player_connection':
            # Define type of entity from num of users connected
            entity_type         = len(users_connected) % 4
            # Send to client:
            #       -> window_size
            #       -> matrix
            #       -> coins_collected
            #       -> users_collected
            #       -> Number of rows
            #       -> Number of cols
            #       -> Size block
            #       -> Entity type assigned
            socket.send_multipart([
                ident,
                b'init_user',
                bytes(str(window_size), 'ascii'),
                bytes(str(matrix), 'ascii'),
                bytes(str(coins_collected), 'ascii'),
                bytes(str(users_connected), 'ascii'),
                bytes(str(ROWS), 'ascii'),
                bytes(str(COLS), 'ascii'),
                bytes(str(SIZE_BLOCK), 'ascii'),
                bytes(str(entity_type), 'ascii')
            ])

            # Add new user to users_connected by key[username]
            users_connected[ident] = {
                'points': 0,
                'pos': {
                    'x': 16,
                    'y': 16
                },
                'type': entity_type
            }

            # Create package to share in new user broadcast
            # Package -> [username_new_user, typeEntity, posX, posY]
            dataToShare         = bytes(
                str([ident, entity_type, 16, 16]), 'ascii'
            )
            # Send broadcast to clients connected
            multicastUsers(
                socket,
                users_connected,
                [b'new_user', dataToShare],
                ident
            )

        # If some user eat a coin
        if operation == b'eat_coin':
            # Get bytes of posX in message
            bytesPosX           = rest[0]
            # Get bytes of posY in message
            bytesPosY           = rest[1]

            # Convert posX bytes to str
            posX                = bytesPosX.decode('ascii')
            # Convert posY bytes to str
            posY                = bytesPosY.decode('ascii')

            # Create a key for cookie ate
            key                 = posX + " " + posY

            # Add coin ate and assign to user
            coins_collected[key] = ident
            # Send broadcast to clients connected
            multicastUsers(
                socket,
                users_connected,
                [b'cookie_eated', bytesPosX, bytesPosY, ident],
                ident
            )

        # If some user change of position
        if operation == b'change_position':
            # Get position pair from first info in rest
            pos                 = eval(rest[0])

            # Change user position
            users_connected[ident]['pos'] = {
                'x': pos[0],
                'y': pos[1]
            }

            # Send broadcast to clients connected
            multicastUsers(
                socket,
                users_connected,
                [b'user_change_position', ident] + rest,
                ident
            )

        # If some user kill another
        if operation == b'kill_user':
            # Send broadcast to clients connected
            multicastUsers(
                socket,
                users_connected,
                [b'kill_someone', ident] + rest,
                ident
            )

            user_killed = rest[0]
            del users_connected[user_killed]

# If running proccess require a main
if __name__ == '__main__':
    # Exec main function
    main()
