# Function to user eat a cookie
def eatCookies(coins, posPlayer, socket, coinsCollected, username, userPoints):
    # Create a list of cookies availables
    cookiesAvailables           = []
    # Iter over actual coins
    for coin in coins:
        # Get type of coin
        typeCoin                = coin.typeCoin
        # Get relative position in X of the coin and cast it to int
        posX                    = int(coin.relativePos[0])
        # Create a bytes of relative position X
        bytesPosX               = bytes(str(posX), 'ascii')

        # Get relative position in Y of the coin and cast it to int
        posY                    = int(coin.relativePos[1])
        # Create a bytes of relative position Y
        bytesPosY               = bytes(str(posY), 'ascii')

        # Create a key of cookie to coinsCollected
        key                     = '%i %i' % (posX, posY)

        # Detect if user is over the coin
        userIsOverCoin          = coin.userIsOverCoin(posPlayer)
        # Detect if the coin is available to eat it
        cookieAvailable         = not key in coinsCollected

        # If user is over the coin
        if userIsOverCoin:
            # Send to server that coin has ate by user
            socket.send_multipart([b'eat_coin', bytesPosX, bytesPosY])
            # Add 5 points to user if coin is normal, else add 10 points
            userPoints         += 5 if typeCoin == 'normal' else 10
            # Add cookie key to coinsCollected to refer coin isn't available
            coinsCollected[key] = username

        # If user isn't over the cookie and cookie is available
        elif not userIsOverCoin and cookieAvailable:
            # Add cookie to list of cookies availables
            cookiesAvailables.append(coin)

    # Return cookiesAvailables, coinsCollected, userPoints
    return cookiesAvailables, coinsCollected, userPoints
