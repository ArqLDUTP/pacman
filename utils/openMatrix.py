# Function to read a file of matrix and get:
# -> window_size
# -> matrix (-|c|C)
# -> number of rows
# -> number of cols
def openMatrix(pathToMatrixResource, sizeBlock):
    # Open file matrix
    with open(pathToMatrixResource, 'r') as file:
        # Read a matrix file
        matrix =  file.readlines()

        # Init cols and rows
        rows = len(matrix)
        cols = len(matrix[0])-1

        # Iter over matrix to remove '|\n' in lines
        for index in range(0, len(matrix)):
            matrix[index] = matrix[index].replace('|\n', '').replace(' ', 'c')

        # Recalculate number of cols and rows
        rows = len(matrix)
        cols = len(matrix[0])

        # Define a pixel size width
        screen_width = sizeBlock*cols
        # Define a pixel size height
        screen_height = sizeBlock*rows
        # Create a pair object
        window_size = (screen_width, screen_height)

        # Return data
        return window_size, matrix, rows, cols
