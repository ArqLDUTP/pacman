# Import pygame
import pygame

# Coin
class Coin(object):

    # Constructor
    #   -> posX         -> X position of coin
    #   -> posY         -> Y position of coin
    #   -> color        -> color of coin
    #   -> SIZE_BLOCK   -> SIZE_BLOCK
    #   -> typeCoin     -> typeCoin of coin
    def __init__(self, posX, posY, color, SIZE_BLOCK, typeCoin):
        # Set SIZE_BLOCK
        self.SIZE_BLOCK = SIZE_BLOCK
        # Set typeCoin (c -> 'normal', C -> 'power')
        self.typeCoin = typeCoin
        # Create real position
        self.relativePos = (posX/SIZE_BLOCK, posY/SIZE_BLOCK)
        # Put coin on middle of square
        self.pos = (
            int(posX + SIZE_BLOCK/2),
            int(posY + SIZE_BLOCK/2)
        )
        # Set posX
        self.posX = posX
        # Set posY
        self.posY = posY
        # Set posY
        self.color = color
        # Set big radius if coin is power, or 2 if coin is normal
        self.radius = 2 if typeCoin == 'normal' else 5

    # Method to know if user if over coind
    def userIsOverCoin(self, posUser):
        # If user is in posX
        inPosX = self.relativePos[0] == posUser[0]
        # If user is in posY
        inPosY = self.relativePos[1] == posUser[1]
        # Return if user is in X and Y pos
        return inPosX and inPosY

    # Method draw in screen
    def draw(self, screen):
        # Create a circle in screen
        pygame.draw.circle(screen, self.color, self.pos, self.radius)
