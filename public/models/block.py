# Import pygame
import pygame

# Class block
class Block(pygame.sprite.Sprite):

    # Constructor
    #   -> x        -> X position of block
    #   -> y        -> Y position of block
    #   -> width    -> width of block
    #   -> height   -> height of block
    #   -> color    -> color of block
    def __init__(self, x, y, width, height, color):
        # Call Sprite's constructor
        super().__init__()
        # Create a Surface
        self.image = pygame.Surface((width, height))
        # Full surface of color
        self.image.fill(color)
        # Get rect of surface
        self.rect = self.image.get_rect(topleft=(x, y))
