# Function to define a new position of user defined by new vector key
# and if the new position isn't a block
def getNewPosPlayer(actualPosition, matrix, arrowPressed, ROWS, COLS):
    # Get actual X position
    posX            = actualPosition[0]
    # Get actual Y position
    posY            = actualPosition[1]

    # If new vector is up, and the position above isn't a block (-)
    if arrowPressed == 'up' and posY > 0 and matrix[posY-1][posX] != '-':
        # Reduce a Y position to up user
        posY        -= 1
        # Return new position pair
        return (posX, posY)

    # If new vector is down, and the position bloew isn't a block (-)
    elif arrowPressed == 'down' and posX < ROWS and matrix[posY+1][posX] != '-':
        # Increment a Y position to down user
        posY        += 1
        # Return new position pair
        return (posX, posY)

    # If new vector is right, and the position in the right isn't a block (-)
    elif arrowPressed == 'right' and posX < COLS and matrix[posY][posX+1] != '-':
        # Increment a X position to right user
        posX        += 1
        # Return new position pair
        return (posX, posY)

    # If new vector is left, and the position in the left isn't a block (-)
    elif arrowPressed == 'left' and posX > 0 and matrix[posY][posX-1] != '-':
        # Reduce a X position to left user
        posX        -= 1
        # Return new position pair
        return (posX, posY)

    # If in some case, the new position is a block
    # Return normal position
    return (posX, posY)
