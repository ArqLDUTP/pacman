# Import pygame
import pygame

# Function to remove a user killed from list
def killPlayer(players, entities, userKilled):
    # Create a temp dict of players
    tempPlayers                         = {}
    # Create a temp Group of entities
    tempEntities                        = pygame.sprite.Group()

    # Iter over players
    for playerUsername in players:
        # Get username of player
        player                          = players[playerUsername]
        # If user killed isn't actual player
        if playerUsername != userKilled:
            # Add player alive to tempPlayers
            tempPlayers[playerUsername] = players[playerUsername]
            # Add player alive to tempEntities
            tempEntities.add(tempPlayers[playerUsername])

    # Return tempPlayers, tempEntities
    return tempPlayers, tempEntities
