# Function to draw some model in screen
def drawModel(screen, listModels):
    # Iter over list modelss
    for model in listModels:
        # Use draw method over screen
        model.draw(screen)
