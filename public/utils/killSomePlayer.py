# Import pygame
import pygame

# Fucntion to detect if in the new position of player, some player was killed
def killSomePlayer(players, entities, username, socket, userPoints):
    # Create a temp dict of players
    tempPlayers = {}
    # Create a temp Group of entities
    tempEntities = pygame.sprite.Group()

    # Get actual position of player
    posUser = players[username].get_position()

    # Iter over players
    for playerUsername in players:
        # Get username in bytes of player
        playerUsernameBytes = bytes(playerUsername, 'ascii')
        # Get username of player
        player = players[playerUsername]

        # If player getted is me
        if playerUsername == username:
            # Add player to tempPlayers
            tempPlayers[playerUsername] = players[playerUsername]
            # Add player to entities to draw
            tempEntities.add(player)
        # else if player was in that position
        elif posUser == player.get_position():
            # Eat the player
            # Add 50 points to player
            userPoints += 50
            # Send to server the kill
            socket.send_multipart([b'kill_user', playerUsernameBytes])
        else:
            # Add player to tempPlayers
            tempPlayers[playerUsername] = players[playerUsername]
            # Add player to entities to draw
            tempEntities.add(tempPlayers[playerUsername])

    # Return tempPlayers, tempEntities, userPoints
    return tempPlayers, tempEntities, userPoints
