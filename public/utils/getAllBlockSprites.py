# Import Block model
from models.block import Block
# Import Coin model
from models.coin import Coin
# Import pygame
import pygame

# Function to get blocks and coins from matrix
def getAllBlockSprites(matrix, colorBlock, colorCoin, SIZE_BLOCK):
    # Create a group of blocks
    blocks                  = pygame.sprite.Group()
    # Create a empty list of coins
    coins                   = []
    # Init in 0 the row index to indicate a real position in Y
    rowIndex                = 0
    # Iter over matrix
    for row in matrix:
        # Init in 0 the row index to indicate a real position in X
        colIndex            = 0
        # Iter over row matrix
        for element in row:
            # If element is a -, is a block
            if element == '-':
                # Add block object into blocks group
                blocks.add(
                    Block(
                        colIndex,rowIndex,
                        SIZE_BLOCK,SIZE_BLOCK,
                        colorBlock
                    )
                )
            # If element is a c, is a normal coin
            elif element == 'c':
                # Add normal coin object to coins list
                coins.append(
                    Coin(colIndex, rowIndex, colorCoin, SIZE_BLOCK, 'normal')
                )
            # If element is a C, is a power coin
            elif element == 'C':
                # Add power coin object to coins list
                coins.append(
                    Coin(colIndex, rowIndex, colorCoin, SIZE_BLOCK, 'power')
                )

            # Add size block to col index
            colIndex        += SIZE_BLOCK

        # Add size block to row index
        rowIndex            += SIZE_BLOCK

    # Return block group and coins list
    return blocks, coins
