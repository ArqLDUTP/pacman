# Function to send a data to multiple users
def multicastUsers(socket, users_connected, dataList, identSocket):
    # Iter over clients connecteds
    for dest in users_connected:
        # If the dest is the same user that send the multicast
        if dest != identSocket:
            # Add dest in first data to send position
            dataToSend = [dest] + dataList
            # Send to dest client
            socket.send_multipart(dataToSend)
